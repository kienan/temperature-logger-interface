= Debian 8 (Jessie) =

*Note*: This is not the recommended way of doing it. Ideally, there should be a virtual environment (venv) setup.

sudo apt-get install python-serial python-numpy python-matplotlib python-pip python-dev

sudo pip install cython
sudo apt-get install libgl1-mesa-dev \
    libsdl-image1.2-dev \
    libsdl-mixer1.2-dev \
    libsdl-ttf2.0-dev \
    libsmpeg-dev \
    libsdl1.2-dev \
    libportmidi-dev \
    libswscale-dev \
    libavformat-dev \
    libavcodec-dev \
    zlib1g-dev
sudo pip install kivy==1.9 --upgrade
