Interface for the arduino temperature logger. It is meant to be run on a machine connected to the arduino.

# Installation #

Requires Python 2.7+

The packages listed below are the  dependencies for both the cli parser & the kivy-based UI.

## Python Packages ##

*The easiest way to get these is to use pip or a similar tool, eg. your distro's repo if the versions are recent enough*

  * pyserial
  * numpy
  * kivy 1.9+ (see installation instructions for kivy)
  * pygame / PIL
  * matplotlib 1.4.3+

## Detailed instructions on debian 8 (jessie, lxde install) ##

  * sudo apt-get install python python-dev python-pip libgl1-mesa-dev
  * sudo pip Cython==0.21.2 pyserial numpy matplotlib kivy==1.9

### Command-line only ###

 * sudo apt-get install python python-pip
 * sudo pip install pyserial

# Icons #

The graphical interface uses icons from the Simplicio icon set by Neurovit: http://neurovit.deviantart.com/
